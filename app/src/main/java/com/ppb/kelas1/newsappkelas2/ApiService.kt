package com.ppb.kelas1.newsappkelas2
import com.ppb.kelas1.newsappkelas2.data.NewsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
interface ApiService {
    @GET("everything")
    fun getNews(
        @Query("q") search: String,
        @Query("from") from: String,
        @Query("language") language: String,
        @Query("apiKey") apiKey: String,
    ): Call<NewsResponse>
}