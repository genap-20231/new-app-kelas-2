package com.ppb.kelas1.newsappkelas2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.ppb.kelas1.newsappkelas2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModels()
    private val adapter = ArticleAdapter()
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //init recycler view
        binding.rvArticles.layoutManager = LinearLayoutManager(this)
        binding.rvArticles.adapter = adapter

        //observe newsResponse di viewModel
        viewModel.newsResponse.observe(this) { newsResponse ->
            adapter.articles = newsResponse.articles
        }

        viewModel.getNewsFromAPI("jokowi", "2024-04-10")
    }
}