package com.ppb.kelas1.newsappkelas2.data

data class NewsResponse(
    val status: String,
    val totalResults: Int,
    val articles: List<Article>
)
